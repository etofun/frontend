# [etofun-frontend](https://etofun.gitlab.io/frontend/)

```bash
yarn install    # Project setup
yarn serve      # Compiles and hot-reloads for development
yarn build      # Compiles and minifies for production
yarn test:unit  # Run your unit tests
yarn test:e2e   # Run your end-to-end tests
yarn lint       # Lints and fixes files
```
