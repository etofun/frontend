/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');

/**
 * @see https://cli.vuejs.org/ru/config
 */
module.exports = {
  runtimeCompiler: true,
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME
    : '/',
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      // load which style file you want to import globally
      patterns: [path.resolve(__dirname, './src/assets/scss/_variables.scss')],
    },
  }
};