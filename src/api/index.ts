import { AxiosInstance } from 'axios';

import admin from './admin';

export default (axios: AxiosInstance) => ({
  admin: admin(axios),
});
