import { AxiosInstance } from 'axios';

export default (axios: AxiosInstance) => ({
  getItem(url: string, id: number) {
    return axios.get(`${url}/${id}`);
  },

  postItem(url: string, data: any) {
    return axios.post(`/${url}`, data);
  },

  patchItem(url: string, id: number, data: any) {
    return axios.patch(`/${url}/${id}`, data);
  },

  deleteItem(url: string, id: number) {
    return axios.delete(`/${url}/${id}`);
  },
});
