import { Plugin } from 'vue'
import axios, { AxiosInstance, AxiosStatic } from "axios";
import apiModules from '@/api';

const apiPlugin: Plugin = {
  install(app, axiosConfig = {}) {
    const apiClient: AxiosInstance = axios.create({
      baseURL: '/api',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      ...axiosConfig,
    });

    app.config.globalProperties.$api = apiModules(apiClient);
  }
}

export default apiPlugin;

export { apiPlugin, apiModules, AxiosStatic, AxiosInstance }
