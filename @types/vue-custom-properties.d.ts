import { apiModules, AxiosStatic } from '@/plugins/api';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $api: AxiosStatic & ReturnType<typeof apiModules>,
  }
}
