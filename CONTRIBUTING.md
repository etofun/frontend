 # Contributing to ETOFUN

We are using git for version management and control. We use gitlab for
issue and milestone tracking.

We classify contributions as *Major* or *Minor*.

A Major contribution would be adding a new scheme, or capability to
the ETOFUN. Modifications that would require users to change existing
code are also considered Major and would not be scheduled for inclusion 
except for a Major release. We usually require such contributions to be 
done in their own fork of the repository to minimize disruption to the
ongoing release schedule.

Minor contributions are less broad in scope, and usually are limited
to a few files at a time. These are usually done on a branch in the
development repository, and are usuall incorporated into the next
minor release cycle.

Sometimes a seemingly minor improvement may affect a large number of
files. Formatting changes are an example of this. Changes to a large
number of files can be disruptive if done in the wrong point of a
release cycle.

If you discover a problem or identify a useful enhancement, do feel
free to create a new issue in github. Major enhancements should be
discussed with the ETOFUN team ahead of time before undertaking any
work (see below).

# Workflow for Minor Contributions

Our workflow for Minor contributions is that developers work in
feature branches they create and then submit merge requests. All
contributions -- be they bug fixes or enhancements -- must be
documented in a gitlab issue, before the merge request.

## Making the code changes and checking in the result

We request that you conform to the following workflow:
1. Start in master, or whichever branch you want to start from using the following command: ```git checkout master```
2. Pull down the latest in this branch from the git repo: ```git pull origin master```
3. Create a new branch with the a unique name: ```git checkout -b <your new branch name>```.
    * Note that we recommend naming feature branches by appending your name with an issue number.  If your last name is Rohloff and you're fixing a bug documented in issue 233, then one would create a branch named Rohloff_issue233.
    * This command will create the branch and move you into it.

4. Make any changes you want to in the branch.
5. Commit your changes to the local repo: ```git commit -am "commit message"```
    * Note the commit message should be succinct yet meaningful and indicate the issue you're addressing, and discussion of things you weren't able to address.
    * Be sure the `pre-commit` hooks run, to ensure the code meets the style guidelines.
    * For a more granular control, you can first add files using `git add` and then run `git commit -m "commit message"`. In this case, the changes made by pre-commit will not automatically be added to the commit. Review the changes using `git diff`. If all looks well, run `git add`, and then retry `git commit -m "commit message"`.
6. Push your local commit to the server in your branch: ```git push origin <your local branch name>```

7. After you finished inserting your new code you wanted to address, make sure the code builds and runs correctly and that you have not introduced any additional bugs.
8. Make sure all unit tests pass and add additional unit tests as needed for features you've added.
9. Before creating merge requests, developers should rebase their branch from master and test that their code works properly.
10. Submit a merge request so project owners can review your commits here. You should include the text
```Link #issue``` in your merge request.
11. You may get feedback on your merge request, especially if there are problems or issues.
12. When your merge request is accepted, your changes will be merged into master and your branch will be deleted.


# Style Guidelines

* Try to follow the style of surrounding code, and use variable names that
  follow existing patterns. Pay attention to indentation and spacing.
* Configure your editor to use 4 spaces per indentation level, and **never to
  use tabs**.
* Avoid introducing trailing whitespace
* Limit line lengths to 80 characters when possible
* Write comments to explain non-obvious operations within the code, both in header or source files.
* Write Doxygen style comments to define all Classes, Templates, and
  methods (both public, private and protected. Please document all
  input and output data characterisitcs (required lengths of vectors,
  restrictions on combinations of variables) as well as any conditions
  that generate exceptions.

# Acknowlegement

We would like to Acknowlege the PALISADE. We have modeled this
document on their examples.
